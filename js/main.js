(function () {
    let select = () => {
        let selectHeader = document.querySelectorAll('.select__header');
        let selectItem = document.querySelectorAll('.select__item');

        selectHeader.forEach(item => {
            item.addEventListener('click', selectToggle)
        });

        selectItem.forEach(item => {
            item.addEventListener('click', selectChoose)
        });

        function selectToggle() {
            this.parentElement.classList.toggle('is-active');
        }

        function selectChoose() {
            let select = this.closest('.select');
            let hidden_input = select.querySelector('.hidden-input');
            let currentText = select.querySelector('.select__current');
            currentText.innerText = this.innerText;
            hidden_input.value = this.innerText;
            select.classList.remove('is-active');
        }
    };
    select();
    document.querySelector('.adaptive-menu-btn').onclick = () => {
        document.querySelector('.header nav').classList.toggle('shown')
    }

})();